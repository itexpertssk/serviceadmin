# Description

Docker file with instructions to build the service container.

## Instructions for building the Docker image
 `docker build --rm=true --tag=itexperts/serviceadmin .`

## Instructions for starting Docker container
 `docker run -it -p 8800:8800 --name serviceadmin itexperts/serviceadmin`

 
## How to start JVM with Jolokia agent
* Use JOLOKIA_AGENT_OPTS with java command
  `java $JOLOKIA_AGENT ...`
  or,
* Implement java wrapper script which constructs the JOLOKIA_AGENT_OPTS variable. 
```
JOLOKIA_AGENT_OPTS="-javaagent:/opt/jolokia/${JOLOKIA_AGENT_JAR}=port=${JOLOKIA_PORT},host=${JOLOKIA_HOST},user=${JOLOKIA_USER},\
password=${JOLOKIA_PASSWORD},protocol=${JOLOKIA_PROTOCOL}"
```

You can customize Jolokia agent with following Environment variables.

| Variable | Description | Default value |
| -------- | ----------- | ------------- |
|JOLOKIA_HOST|Hostaddress to which the HTTP server should bind to|0.0.0.0|
|JOLOKIA_PORT|Port the HTTP server should listen to|8778|
|JOLOKIA_USER|User to be used for authentication|jolokia|
|JOLOKIA_PASSWORD|Password used for authentication|jolokia|
|JOLOKIA_PROTOCOL|HTTP protocol to use. Should be either http or https|http|
