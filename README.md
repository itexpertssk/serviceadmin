# serviceadmin
Useful community project which provides insights into running microservices.
More info: https://github.com/codecentric/spring-boot-admin 

## Try it
* Spring Boot Admin GUI: http://localhost:8800  (admin/password) 